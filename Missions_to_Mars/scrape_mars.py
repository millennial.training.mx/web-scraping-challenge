import pandas as pd
from bs4 import BeautifulSoup
from splinter import Browser

def scrape():
    executable_path = {"executable_path": "/usr/bin/chromedriver"}
    browser = Browser("chrome", **executable_path, headless=False)

    #scrape the top article's title and headline from NASA news
    url = "https://mars.nasa.gov/news/"
    browser.visit(url)
    html = browser.html
    soup = BeautifulSoup(html, "html.parser")
    top_title = browser.find_by_css(".content_title")[0].text
    top_headline = browser.find_by_css(".article_teaser_body")[0].text
    print(top_title)
    print(top_headline)

    #Get featured image from NASA JPL Mars Space Images
    url = "https://jpl.nasa.gov/spaceimages/?search=&category=Mars"
    browser.visit(url)
    html = browser.html
    soup = BeautifulSoup(html, "html.parser")
    featured_img = "https://jpl.nasa.gov" + soup.find(class_ = "carousel_item")['style'].split("'")[1]
    print(featured_img)

    # Get latest tweet from Mars Weather on twitter
    weather = browser.find_by_css(".TweetTextSize")[0].text.strip()
    print(weather)

    #create a table of Space Facts about Mars
    url = "https://space-facts.com/mars/"
    browser.visit(url)
    html = browser.html
    soup = BeautifulSoup(html, "html.parser")
    table = soup.find_all('table')[0]
    # Use pandas to render to table
    mars_facts_html = pd.read_html(table)
    print(mars_facts_html)


    #Get high-res images of each Martian hemispheres
    url = "https://astrogeology.usgs.gov/search/results?q=hemisphere+enhanced&k1=target&v1=Mars"
    browser.visit(url)
    html = browser.html
    soup = BeautifulSoup(html, "html.parser")
    titles = soup.find_all('h3')
    hems_titles = [item.text.strip().replace(' Enhanced','') for item in titles]
    hems_urls = set( ["https://astrogeology.usgs.gov" + item['href'] for item in soup.find_all('a', class_ = 'itemLink product-item')] )
    hems_imgs = []
    for url in hems_urls:
        browser.visit(url)
        html = browser.html
        soup = BeautifulSoup(html, "html.parser")
        img_src = "https://astrogeology.usgs.gov" + soup.find('img', class_ = "wide-image")['src']
        hems_imgs.append(img_src)
    hems_data = []
    i=1
    while i < 4:
        hems_data.append(
            {'title': hems_titles[i], 'image_url': hems_imgs[i]}
        )
        i += 1
    print(hems_data)

    scrape_data = {
        'top_title' : top_title,
        'top_headline' : top_headline,
        'featured_img' : featured_img,
        'weather' : weather,
        'mars_facts_html' : mars_facts_html,
        'hems_data' : hems_data,
    }
    return scrape_data


if __name__ == "__main__":
    print( scrape() )