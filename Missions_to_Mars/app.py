#!/usr/bin/env python
# coding: utf-8

# In[ ]:


from flask import Flask, render_template, jsonify, redirect
from flask_pymongo import PyMongo
import scrape_mars

# Create an instance of Flask
app = Flask(__name__)

# Use PyMongo to establish Mongo connection
mongo = PyMongo(app, uri="mongodb://localhost:27017/mars_db")

# Route to index page to render Mongo data to template
@app.route("/")
def index():
    # get data from Mongo
    data = mongo.db.mars_data.find_one()
    # render template for page
    return render_template("index.html", mars_data=data)
    
@app.route("/scrape")
def scrape():
    # scrape for Mars news, facts, images and data
    mars_data = scrape_mars.scrape()

    # mars.append
    mongo.db.mars_data.update({}, mars_data, upsert = True)

    # Redirect back to home page
    return redirect("/")

if __name__ == "__main__":
    app.run(debug=True)

