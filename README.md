# web-scraping-challenge

## Used Python, Flask, MongoDB, HTML, and CSS to produce a website with up to date news on Mars
* Python script employing splinter and BeautifulSoup to scrape current Mars news and high-resolution photos from NASA's JPL website, NASA's latest tweet about Mars from Twitter, and a table of facts about Mars from space-facts.com.
* created a MongoDB database to hold the image urls and text
* used Bootstrap to create a web page that displays the images and text
* wrote a Flask app to pull data from MongoDB and display in an HTML page
* added button to refresh data by scraping from sources and persisting the data to mongodb

Screenshot of the webpage:

# Mission to Mars Info Website
![Mars Website](Missions_to_Mars/mission_to_mars_ss.png)